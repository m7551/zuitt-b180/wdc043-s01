package com.zuitt;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = scannerName.nextLine();
        System.out.println("Last Name:");
        lastName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = scannerName.nextDouble();
        System.out.println("Second Subject Grade:");
        secondSubject = scannerName.nextDouble();
        System.out.println("Third Subject Grade:");
        thirdSubject = scannerName.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + "!");

        double average = ((firstSubject + secondSubject + thirdSubject) / 3);
        int grade = (int) average;
        System.out.println("Your average grade is: " + grade);
    }
}
